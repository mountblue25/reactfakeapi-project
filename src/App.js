import './App.css';
import ProductsContainer from './Components/ProductContainer';

function App() {
  return (
    <div>
      <h1 className='main-heading'>Our Products</h1>
      <ProductsContainer />
    </div>
  );
}

export default App;
