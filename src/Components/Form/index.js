import React, { Component } from "react";
import { Link } from "react-router-dom";
import validator from 'validator';
import './index.css'

export class Form extends Component {
    state = {
        Title: "",
        Category: "",
        Price: "",
        Description: "",
        rating: "",
        Count: "",
        ImageUrl: "",
        TitleError: "",
        CategoryError: "",
        PriceError: "",
        DescriptionError: "",
        ratingError: "",
        CountError: "",
        ImageUrlError: "",
        isCategoryValid: false,
        isPriceValid: false,
        isRatingValid: false,
        isCountValid: false,
        isUrlValid: false,
        isSubmited: false
    }

    onChangeInput = (event) => {
        this.setState({ [event.target.id]: event.target.value })
    }

    CategoryValidation = (event) => {
        if (validator.isAlpha(event.target.value) === true) {
            this.setState({ isCategoryValid: true, CategoryError: " " })
        } else {
            this.setState({ isCategoryValid: false, CategoryError: "*Please Enter valid category" })
        }
    }

    PriceValidation = (event) => {
        if ((validator.isNumeric(event.target.value) === true) && (event.target.value > 0)) {
            this.setState({ isPriceValid: true, PriceError: " " })
        } else {
            this.setState({ isPriceValid: false, PriceError: "*Please enter valid Price" })
        }
    }

    ratingValidation = (event) => {
        if ((validator.isNumeric(event.target.value) === true) && (event.target.value <= 5.0)) {
            this.setState({ isRatingValid: true, ratingError: " " })
        } else {
            this.setState({ isRatingValid: false, ratingError: "*Please Enter Valid rating " })
        }
    }
    CountVlidation = (event) => {
        if (validator.isInt(event.target.value, { min: 0 }) === true) {
            this.setState({ isCountValid: true, CountError: " " })
        } else {
            this.setState({ isCountValid: false, CountError: "* Please Add valid count" })
        }
    }

    UrlValidator = (event) => {
        if (validator.isURL(event.target.value) === true) {
            this.setState({ isUrlValid: true, ImageUrlError: " " })
        } else {
            this.setState({ isUrlValid: false, ImageUrlError: "*Please enter valid url" })
        }
    }

    SubmitValidation = (event) => {
        event.preventDefault()
        const { Title, Category, Price, Description, rating, Count, ImageUrl, isCategoryValid, isPriceValid,
            isRatingValid, isCountValid, isUrlValid } = this.state
        if (Title === "") {
            this.setState({ TitleError: "*Please Enter Title" })
        } else {
            this.setState({ TitleError: " " })
        }
        if (Category === "") {
            this.setState({ CategoryError: "*Please enter category" })
        }
        if (Price === "") {
            this.setState({ PriceError: "*Please enter valid Price" })
        }
        if (Description === "") {
            this.setState({ DescriptionError: "*Please enter Description" })
        } else {
            this.setState({ DescriptionError: " " })
        }
        if (rating === "") {
            this.setState({ ratingError: "*Please enter rating" })
        }
        if (Count === "") {
            this.setState({ CountError: "*Please enter Count" })
        }
        if (ImageUrl === "") {
            this.setState({ ImageUrlError: "*Please enter ImageUrl" })
        }
        if ((Title !== "") && (Category !== "") && (Price !== "") && (Description !== "") && (rating !== "") && (Count !== "")
            && (ImageUrl !== "") && (isCategoryValid) && (isPriceValid) && (isCountValid) && (isRatingValid) &&
            (isUrlValid)) {
            this.setState({ isSubmited: true })
            this.props.gettingData(this.state)
        }


    }

    render() {
        const { isSubmited } = this.state

        return (
            <div className="Form-container">
                <form onSubmit={this.SubmitValidation}>
                    <div className="input-container">
                        <label className="label">Title</label><br />
                        <input type="text" id="Title" className="Input-text" onChange={this.onChangeInput} /><br />
                        <span className="Error">{this.state.TitleError}</span>
                    </div>
                    <div className="input-container">
                        <label className="label">Category</label><br />
                        <input type="text" id="Category" className="Input-text" onChange={this.onChangeInput} onBlur={this.CategoryValidation} /><br />
                        <span className="Error">{this.state.CategoryError}</span>
                    </div>
                    <div className="input-container">
                        <label className="label">Description</label><br />
                        <textarea type="text" id="Description" className="Input-textarea" rows="10" cols="30" onChange={this.onChangeInput} > </textarea><br />
                        <span className="Error">{this.state.DescriptionError}</span>
                    </div>
                    <div className="input-container">
                        <label className="label">Price</label><br />
                        <input type="text" id="Price" className="Input-text" onChange={this.onChangeInput} onBlur={this.PriceValidation} /><br />
                        <span className="Error">{this.state.PriceError}</span>
                    </div>
                    <div className="input-container">
                        <label className="label">Rating</label><br />
                        <input type="text" id="rating" className="Input-text" onChange={this.onChangeInput} onBlur={this.ratingValidation} /><br />
                        <span className="Error">{this.state.ratingError}</span>
                    </div>
                    <div className="input-container">
                        <label className="label">Count</label><br />
                        <input type="text" id="Count" className="Input-text" onChange={this.onChangeInput} onBlur={this.CountVlidation} /><br />
                        <span className="Error">{this.state.CountError}</span>
                    </div>

                    <div className="input-container">
                        <label className="label">ImageUrl</label><br />
                        <input type="text" id="ImageUrl" className="Input-text" onChange={this.onChangeInput} onBlur={this.UrlValidator} /><br />
                        <span className="Error">{this.state.ImageUrlError}</span>
                    </div>
                    <div className="btn-container" style={isSubmited ? { display: "none" } : { display: "block" }}>
                        <button className="signup-btn" type="Submit" onClick={this.SubmitValidation}>Add Product</button>
                    </div>
                    <div className="btn-container" style={isSubmited ? { display: "block" } : { display: "none" }}>
                        <Link to="/">
                            <button className="signup-btn" >Go To Home</button>
                        </Link>
                    </div>
                </form>
            </div>
        )
    }
}


export default Form