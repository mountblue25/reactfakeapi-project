import React, { Component } from 'react'
import axios from 'axios';
import Loader from 'react-loader-spinner'
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'
import './index.css'
import ErrorItem from '../Error';

export class Descriptipon extends Component {
  state = { product: {}, clickedId: window.location.pathname.slice(1), isFetched: false }

  componentDidMount() {
    this.GetItemData()
  }

  GetItemData() {
    const { clickedId } = this.state
    if (clickedId <= 20) {
      axios.get('https://fakestoreapi.com/products')
        .then((response) => {
          return response.data
        })
        .then((data) => {
          const reqProduct = data.filter((product) => {
            return product.id == clickedId
          })
          reqProduct.length > 0 ?
            this.setState({ product: reqProduct[0], isFetched: true })
            :
            this.setState({ product: {}, isFetched: true })
        })
        .catch((error) => {
          this.setState({ product: {}, isFetched: true })
          console.log(error);
        })
    } else {
      console.log(this.props.data.products);
      const reqProduct = this.props.data.products.filter((product) => {
        return product.id == clickedId
      })
      reqProduct.length > 0 ?
        this.setState({ product: reqProduct[0], isFetched: true })
        :
        this.setState({ product: {}, isFetched: true })
    }

  }

  renderingElements = () => {
    return (
      Object.keys(this.state.product).length > 0 ?
        (<div className='product-details-container'>
          <div className='container-1'>
            <img src={`${this.state.product.image}`} className="description-image" /><br />
            <button className='cart-btn'>Add To Cart</button>
            <button className='buy-btn'>Buy Now</button>
          </div>
          <div className='container-2'>
            <h4 className='title-description'>{this.state.product.title}</h4>
            <p className='rating-description'>rating: <span>{this.state.product.rating.rate}</span>&#9733;</p>
            <h2 className='pricing-description'>$ <span>{this.state.product.price}</span></h2>
            <h4 className='category-description'>{this.state.product.category}</h4>
            <p className='description-about-product'>{this.state.product.description}</p>
            <p className='availablity'>Products Available: <span>{this.state.product.rating.count}</span> Only</p>
          </div>
        </div>) :
        <ErrorItem />)
  }

  render() {
    const { isFetched } = this.state
    return (isFetched === false) ? (<Loader type="TailSpin" color="#051c33" height={50} width={50} className="LoaderEl" key="loader" />)
      :
      (<div>
        {this.renderingElements()}
      </div>)
  }
}

export default Descriptipon
