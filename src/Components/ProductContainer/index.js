import React, { Component } from 'react'
import axios from 'axios';
import ProductItem from "../Container";
import ErrorItem from '../Error';
import Description from '../Description'
import Form from '../Form'
import Loader from 'react-loader-spinner'
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom';

export class ProductsContainer extends Component {
    state = { products: [], isFetched: false }

    GettingData = () => {
        axios.get('https://fakestoreapi.com/products')
            .then((response) => {
                this.setState({ products: response.data, isFetched: true })
                return response.data
            })
            .catch((error) => {
                this.setState({ products: [], isFetched: true })
                return error
            })
    }
    
    gettingData = (data)=>{
        console.log();
        let arrangedData = {id: this.state.products.length+1,category: data.Category, description: data.Description, image: data.ImageUrl, price: data.Price,
        rating: {rate: data.rating, count: data.Count}, title: data.Title}

        this.setState({
            products: [...this.state.products,arrangedData]
        }, ()=>{
            console.log(this.state.products);
        })
        return arrangedData
    }

    renderingElements = () => {
        return (
            <div className='products'>
                {this.state.products.length > 0 ?
                    <BrowserRouter>
                        <Routes>
                            <Route exact path="/" element={<ProductItem products={this.state.products} />} />
                            <Route exact path="/addproduct" element={<Form gettingData={this.gettingData} />} />
                            <Route exact path="/:id" element={<Description data={this.state} />} />
                        </Routes>
                    </BrowserRouter>
                    :
                    <ErrorItem />}
            </div>)
    }
    componentDidMount() {
        this.GettingData()
    }
    render() {
        return (this.state.isFetched === false) ? (<Loader type="TailSpin" color="#051c33" height={50} width={50} className="LoaderEl" key="loader" />)
            :
            (<div>
                {this.renderingElements()}
            </div>)
    }
}

export default ProductsContainer