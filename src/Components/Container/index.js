import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './index.css'

export class ProductItem extends Component {
    constructor(props) {
        super(props)
        this.state = { products: this.props.products }
    }
    render() {
        const { products } = this.state
        return <div>
            <Link to="/addproduct"><button className='product-adding-button'>&#43;AddProduct</button></Link>
            <ul className='unordered'>
                {products.map((product) => {
                    return (<Link to={`/${product.id}`} className='card-Element' key={product.id}>
                        <div>
                            <img src={product.image} className="product-image" />
                            <div>
                                <h4 className='product-title'>{product.title}</h4>
                                <p className='product-description'>{product.description}</p>
                                <h4 className='product-category'>{product.category}</h4>
                                <h2 className='product-price'>$ <span>{product.price}</span></h2>
                                <p className='product-rating'>rating: <span>{product.rating.rate}</span></p>
                            </div>
                        </div>
                    </Link>)
                })
                }
            </ul>
        </div>
    }
}

export default ProductItem