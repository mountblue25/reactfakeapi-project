import React, { Component } from 'react'
import './index.css'

export class ErrorItem extends Component {
  render() {
    return (
      <div>
        <h1 className='Error-heading'>Unable to fetch the Data</h1>
        <h3 className='Error-suggetion'>Please Try Again</h3>
        <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRl9DN9irguvWfsYs5L3jiXISDvycmclpp1HQ&usqp=CAU' />
      </div>
    )
  }
}

export default ErrorItem
